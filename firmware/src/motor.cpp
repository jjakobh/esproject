#include "motor.h"

#include <Arduino.h>

Motor::Motor(int pinEnable, int pinIn1, int pinIn2) {
    this->pinEnable = pinEnable;
    this->pinIn1 = pinIn1;
    this->pinIn2 = pinIn2;
}
void Motor::setup() {
    pinMode(pinEnable, OUTPUT);
    pinMode(pinIn1, OUTPUT);
    pinMode(pinIn2, OUTPUT);
    this->setSpeed(255);
    this->turnOff();
}

void Motor::setSpeed(int speed) {
    analogWrite(pinEnable, speed);
}

void Motor::turnOff() {
    digitalWrite(pinIn1, LOW);
    digitalWrite(pinIn2, LOW);
}

void Motor::turnOnForward() {
    digitalWrite(pinIn1, HIGH);
    digitalWrite(pinIn2, LOW);
}

void Motor::turnOnBackwards() {
    digitalWrite(pinIn1, LOW);
    digitalWrite(pinIn2, HIGH);
}