#include <Arduino.h>
#include <HCSR04.h>

#include "motor.h"
#include "webserver.h"

const char* SSID = "ESP-Wifi";
// const char* PASSPHRASE = "passwd";

IPAddress IP(192, 168, 1, 1);
IPAddress MASK(255, 255, 255, 0);

gpio_num_t MOTOR1_ENABLE = GPIO_NUM_12;
gpio_num_t MOTOR1_PIN1 = GPIO_NUM_14;
gpio_num_t MOTOR1_PIN2 = GPIO_NUM_27;

gpio_num_t MOTOR2_ENABLE = GPIO_NUM_33;
gpio_num_t MOTOR2_PIN1 = GPIO_NUM_26;
gpio_num_t MOTOR2_PIN2 = GPIO_NUM_25;

gpio_num_t ULTRASONIC_TRIGGER = GPIO_NUM_2;
gpio_num_t ULTRASONIC_ECHO = GPIO_NUM_15;

extern Motor motor1;
Motor motor1(MOTOR1_ENABLE, MOTOR1_PIN1, MOTOR1_PIN2);

extern Motor motor2;
Motor motor2(MOTOR2_ENABLE, MOTOR2_PIN1, MOTOR2_PIN2);

HCSR04 ultrasonic(ULTRASONIC_TRIGGER, ULTRASONIC_ECHO);

void setup() {
    Serial.begin(9600);

    setupWifi(SSID, IP, MASK);
    configureWebserver();

    motor1.setup();
    motor2.setup();
}

void loop() {
}
