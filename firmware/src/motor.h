#include <stdint.h>

class Motor {
   private:
    int pinEnable;
    int pinIn1;
    int pinIn2;

   public:
    Motor(int pinEnable, int pinIn1, int inPin2);

    void setup();
    void setSpeed(int speed);
    void turnOff();
    void turnOnForward();
    void turnOnBackwards();
};