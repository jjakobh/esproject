#include <ArduinoJson.h>
#include <AsyncJson.h>
#include <ESPAsyncWebServer.h>
#include <WiFi.h>
#include <motor.h>

extern const char index_html[] asm("_binary_src_embedded_index_html_start");
extern const char vehicle_js[] asm("_binary_src_embedded_vehicle_js_start");

extern Motor motor1;
extern Motor motor2;

void setupWifi(const char* ssid, IPAddress ip, IPAddress mask) {
    // setup wifi in as access point
    WiFi.mode(WIFI_MODE_AP);
    WiFi.softAPConfig(ip, ip, mask);
    WiFi.softAP(ssid);

    Serial.println("wifi setup finished");
}

AsyncWebServer server(80);

void translateSpeed(Motor motor, double speed) {
    if (speed > 0) {
        motor.turnOnForward();
    } else if (speed < 0) {
        motor.turnOnBackwards();
    } else if (speed == 0) {
        motor.turnOff();
    }

    if (speed < -1.0 || speed > 1.0) {
        Serial.printf("Speed out of range, clamping to %.2f\n", speed);
    }
    double speedClamped = std::max(-1.0, std::min(1.0, std::abs(speed)));
    int speedInt = (speedClamped * 256.0);

    Serial.printf("[motor] speed=%i\n", speedInt);
    motor.setSpeed(speedInt);
}

void configureWebserver() {
    auto& defaultHeaders = DefaultHeaders::Instance();
    defaultHeaders.addHeader("Access-Control-Allow-Origin", "*");
    defaultHeaders.addHeader("Access-Control-Allow-Methods", "POST, OPTIONS");
    defaultHeaders.addHeader("Access-Control-Allow-Headers", "Content-Type");
    defaultHeaders.addHeader("Access-Control-Max-Age", "240");

    server.on("/", HTTP_GET, [](AsyncWebServerRequest* request) {
        request->send(200, "text/html", index_html);
    });
    server.on("/vehicle.js", HTTP_GET, [](AsyncWebServerRequest* request) {
        request->send(200, "text/javascript", vehicle_js);
    });

    server.on("/api/health", HTTP_GET, [](AsyncWebServerRequest* request) {
        request->send(200, "text/plain", "OK\n");
    });

    server.addHandler(new AsyncCallbackJsonWebHandler("/api/serial/print", [](AsyncWebServerRequest* request, JsonVariant& json) {
        const JsonObject& jsonObj = json.as<JsonObject>();
        JsonString msg = jsonObj["msg"];
        if (!msg) return request->send(400, "text/plain", "expected `msg` field\n");

        Serial.print("Message from /serial/print: ");
        Serial.println(msg.c_str());
        request->send(200);
    }));

    server.addHandler(new AsyncCallbackJsonWebHandler("/api/speed", [](AsyncWebServerRequest* request, JsonVariant& json) {
        const JsonObject& jsonObj = json.as<JsonObject>();
        if (!jsonObj.containsKey("speedLeft") || !jsonObj.containsKey("speedRight")) return request->send(400, "text/plain", "expected `speed` field\n");
        double speedLeft = jsonObj["speedLeft"];
        double speedRight = jsonObj["speedRight"];

        translateSpeed(motor1, speedLeft);
        translateSpeed(motor2, speedRight);

        request->send(200);
    }));

    server.onNotFound([](AsyncWebServerRequest* request) {
        if (request->method() == HTTP_OPTIONS) {
            return request->send(200);
        }
        request->send(404, "text/plain", "Not found");
    });

    server.begin();
}
