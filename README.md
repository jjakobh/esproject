# SCEMbot

## firmware

Die Firmware benutzt den ESP mit Arduino Framework und platform.io zum managen von dependencies.
VSCode hat eine [extension](https://platformio.org/install/ide?install=vscode) für platform.io, mit der man im `firmware` den Code checken und compilen, sowie das Board flashen kann.

Der ESP hat WiFi im `ACCESS_POINT` modus und startet auf `192.168.1.1:80` einen Webserver:

- `GET /api/health`
- `POST /api/serial/print` `{ msg: string }`
- `POST /api/speed` `{ speedLeft: number, speedRight: number }`:
`0.0`: Stopp, `0.0` bis `1.0`: Vorwärts, `-1.0` bis `0.0`: Rückwärts

Außerdem wird auf `/vehicle.js` eine Javascript-Datei ausgegeben, die mit einem `script` tag eingebunden werden kann.

Das Arbeitsblatt befindet sich auf [./Arbeitsblatt.pdf](./Arbeitsblatt.pdf), die Bauanleitung auf [./Bauanleitung.pdf](./Bauanleitung.pdf).
Das Präsentationsvideo findet man auf [./Präsentation.mp4](./Präsentation.mp4).

## js

```js
vehicle.setSpeed(1.0);
await sleep(1000);
vehicle.setSpeed(0.0);
await sleep(1000);

// slalom
for (let i = 0; i < 4; i++) {
    // go left
    vehicle.setSpeed(0.6, 1.0);
    await sleep(delay);

    // go right
    vehicle.setSpeed(1.0, 0.6);
    await sleep(delay);
}
```

Hier das minimale HTML-Dokument zum Steuern des Roboters:
```html
<html>
    <script src="http://192.168.1.1/vehicle.js"></script>
    <script type="module">
        vehicle.setSpeed(1.0);
        await sleep(1000);
    </script>
</html>
```
