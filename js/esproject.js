const BASE_URL = "http://192.168.1.1";

function sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}

function error(e) {
    console.error(e);
}

async function performRequest(path, body) {
    const response = await fetch(`${BASE_URL}/api${path}`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(body),
    });
    if (!response.ok) throw new Error(await response.text());
    return await response.text();
}

async function healthcheck() {
    return await fetch(`${BASE_URL}/api/health`).then((_response) => true).catch(_error => false);
}
async function waitforhealthcheck() {
    while (true) {
        const alive = await healthcheck();
        if (alive) return;

        console.warn("no connection, retrying...");
        await sleep(1000);
    }
}


class Vehicle {
    setSpeed(speed, speed2) {
        if (typeof speed != "number" || !(typeof speed2 != "number" || typeof speed2 != "undefined")) {
            throw new Error("expected number for setSpeed, got " + typeof speed + ", " + typeof speed2);
        }

        let speedLeft = speed;
        let speedRight = speed2 !== undefined ? speed2 : speed;
        console.log("Set speed to " + speedLeft.toFixed(2) + ":" + speedRight.toFixed(2));

        performRequest("/speed", { speedLeft, speedRight }).catch(error);
    }
}

let vehicle = new Vehicle();